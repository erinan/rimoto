defmodule Rimoto.Users.User do
  use Ecto.Schema
  import Ecto.Changeset

  # TODO confirm typing
  @type t :: %__MODULE__{
          id: pos_integer(),
          points: non_neg_integer(),
          inserted_at: DateTime.t(),
          updated_at: DateTime.t()
        }

  @derive {Jason.Encoder, only: [:id, :points]}
  schema "users" do
    field(:points, :integer)

    timestamps(type: :utc_datetime_usec)
  end

  # TEST?
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:points])
    |> validate_number(:points, greater_than_or_equal_to: 0, less_than_or_equal_to: 100)
  end
end
