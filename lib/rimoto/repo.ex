defmodule Rimoto.Repo do
  use Ecto.Repo,
    otp_app: :rimoto,
    adapter: Ecto.Adapters.Postgres
end
