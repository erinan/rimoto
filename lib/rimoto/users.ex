defmodule Rimoto.Users do
  import Ecto.Query

  require Logger

  alias Rimoto.Users.User

  @points %{min_bound: 0, max_bound: 100}
  @default_range @points.min_bound..@points.max_bound

  @spec list_users_with_higher_number(max_points :: non_neg_integer()) :: [User.t()]
  def list_users_with_higher_number(max_points, opts \\ [])
      when max_points >= @points.min_bound and @points.max_bound <= @points.max_bound do
    limit = opts[:limit]

    from(u in User, where: u.points >= ^max_points)
    # note - not sure if this is a "good" way to do this, having a bit of "fun" with then/2!
    |> then(fn query -> if limit, do: limit(query, ^limit), else: query end)
    |> Rimoto.Repo.all()
  end

  @spec update_all_user_points() :: :ok | :error
  def update_all_user_points(batch_size \\ 2000) do
    select_users_query = from u in User, select: u.id

    result =
      Rimoto.Repo.transaction(
        fn ->
          select_users_query
          |> Rimoto.Repo.stream()
          |> Stream.chunk_every(batch_size)
          |> Stream.map(&update_points_for_users/1)
          |> Stream.run()
        end,
        timeout: :infinity
      )

    case result do
      {:ok, _value} ->
        :ok

      error ->
        Logger.error("Failed to update user points - #{inspect(error)}")
        :error
    end
  end

  @spec update_points_for_users(user_ids :: [pos_integer()], updated_time :: DateTime.t()) :: :ok
  def update_points_for_users(user_ids, updated_time \\ DateTime.utc_now()) do
    max_bound = @points.max_bound

    from(u in User,
      where: u.id in ^user_ids,
      update: [set: [points: fragment("random() * ?", ^max_bound), updated_at: ^updated_time]]
    )
    |> Rimoto.Repo.update_all([])

    :ok
  end

  @spec random_number(Range.t()) :: non_neg_integer()
  def random_number(range \\ @default_range), do: Enum.random(range)
end
