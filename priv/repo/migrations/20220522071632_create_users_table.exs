defmodule Rimoto.Repo.Migrations.CreateUsersTable do
  use Ecto.Migration

  def change do
    create table("users") do
      add :points, :integer

      timestamps(type: :utc_datetime_usec)
    end

    # note - interestingly Postgres does not seem to use the index if max_number <= 69 🤔
    create index("users", :points)
  end
end
