max_entries = 1_000_000
batch_size = 2_000

now = DateTime.utc_now()

# TODO add timer?

1..max_entries
|> Stream.map(fn _ -> %{points: Enum.random(0..100), inserted_at: now, updated_at: now} end)
|> Stream.chunk_every(batch_size)
|> Stream.map(fn batch -> Rimoto.Repo.insert_all(Rimoto.Users.User, batch) end)
|> Stream.run()
